# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
import uuid
# Create your models here.


def generate_filename(self, filename):
    url = "escaparates/%s/%s" % (self.escaparate.nombre, filename)
    return url


class Categoria(models.Model):
    nombre = models.CharField(max_length=25)

    def __str__(self):
        return self.nombre


class Provincia(models.Model):
    provincia = models.CharField(max_length=20)

    def __str__(self):
        return self.provincia


class Ciudad(models.Model):
    ciudad = models.CharField(max_length=20)
    provincia = models.ForeignKey(Provincia, null=False, blank=False)

    class Meta:
        verbose_name_plural = "Ciudades"

    def __str__(self):
        return self.ciudad


class ValidateUser(models.Model):
    token = models.CharField(max_length=64, default=uuid.uuid1)
    fechaHora = models.DateTimeField(default=timezone.now, null=True)
    usuario = models.ForeignKey(User, null=False, blank=False)


class Escaparate(models.Model):
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=200)
    direccion = models.CharField(max_length=50)
    ciudad = models.ForeignKey(Ciudad, null=True, blank=True)
    telefono = models.CharField(max_length=50)
    email = models.EmailField(max_length=50)
    paginaweb = models.CharField(max_length=50)
    categoria = models.ForeignKey(Categoria, null=True, blank=True)
    usuario = models.ForeignKey(User, null=False, blank=False)
    puntuacionNumerador = models.DecimalField(max_digits=9, decimal_places=1)
    puntuacionDenominador = models.BigIntegerField()
    calificacion = models.DecimalField(max_digits=9, decimal_places=1)

    def __str__(self):
        return self.nombre


class Publicacion(models.Model):
    titulo = models.CharField(max_length=100)
    descripcion = models.CharField(max_length=1000)
    escaparate = models.ForeignKey(Escaparate, null=False, blank=False)
    fechaHora = models.DateTimeField(default=timezone.now, null=True)
    fecha_baja = models.DateTimeField(null=True)
    imagen = models.ImageField(upload_to=generate_filename, null=True, blank=True)
    tipo_publicacion = [
            (0, "Oculta"),
            (1, "Activa"),
            (2, "Destacada")
        ]

    def __str__(self):
        return self.titulo

    class Meta:
        verbose_name_plural = "Publicaciones"


class Cometario(models.Model):
    comentario = models.CharField(max_length=200)
    usuario = models.ForeignKey(User, null=False, blank=False)
    escaparate = models.ForeignKey(Escaparate, null=True, blank=True)
    publicacion = models.ForeignKey(Publicacion, null=True, blank=True)
    fechaHora = models.DateTimeField(default=timezone.now, null=True)
    fecha_baja = models.DateTimeField(null=True)

    def __str__(self):
        return self.usuario


class Puntuacion(models.Model):
    puntuacion = models.DecimalField(max_digits=2, decimal_places=1)
    usuario = models.ForeignKey(User, null=False, blank=False)
    escaparate = models.ForeignKey(Escaparate, null=False, blank=False)
    fechaHora = models.DateTimeField(default=timezone.now, null=True)
    comentario = models.CharField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.puntuacion

    class Meta:
        verbose_name_plural = "Puntuaciones"


class Denuncia(models.Model):
    motivo = models.CharField(max_length=200, null=False, blank=False)
    usuario = models.ForeignKey(User, null=False, blank=False)
    escaparate = models.ForeignKey(Escaparate, null=False, blank=False)