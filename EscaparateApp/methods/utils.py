# -*- coding: utf-8 -*-
from django.core.mail import send_mail
from EscaparateApp.models import ValidateUser


def sendMail(request, user):
        token = generateToken(user)
        subject = 'Alta usuario'
        message = "Bienvenido " + user.username + "!!! \n "
        message += "Por favor necesitamos que ingrese al siguiente link para validar su cuenta:"
        message += "http://" + request.get_host() + "/validarUsuario/?token=" + str(token)
        sender = 'EscaparateApp@hotmail.com'
        recipients = [user.email]
        send_mail(subject, message, sender, recipients, fail_silently=False)



def generateToken(user):
    validateUser = ValidateUser()
    validateUser.usuario = user
    validateUser.save()
    return validateUser.token