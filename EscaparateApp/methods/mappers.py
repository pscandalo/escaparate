# -*- coding: utf-8 -*-
from EscaparateApp.models import Escaparate, Ciudad, Categoria, Publicacion, Cometario, Puntuacion, Denuncia
from decimal import *


def mapEscaparate(request):
    escaparate = Escaparate.objects.filter(usuario=request.user)

    if escaparate.count() >= 1:
        escaparate = escaparate.first()
    else:
        escaparate = Escaparate()
        escaparate.puntuacionNumerador = 0
        escaparate.puntuacionDenominador = 0
        escaparate.calificacion = 0

    escaparate.nombre = request.POST['nombre']
    escaparate.descripcion = request.POST['descripcion']
    escaparate.direccion = request.POST['direccion']

    if request.POST['ciudad'] != "":
        escaparate.ciudad = Ciudad.objects.filter(id=request.POST['ciudad']).first()
    else:
        escaparate.ciudad = None

    escaparate.telefono = request.POST['telefono']
    escaparate.email = request.POST['email']
    escaparate.paginaweb = request.POST['paginaweb']

    if request.POST['categoria'] != "":
        escaparate.categoria = Categoria.objects.filter(id=request.POST['categoria']).first()
    else:
        escaparate.categoria = None

    escaparate.usuario = request.user
    return escaparate


def mapPublicacion(request):
    if request.POST["id"]:
        publicacion = Publicacion.objects.filter(id=request.POST["id"]).first()
    else:
        publicacion = Publicacion()
        publicacion.escaparate = Escaparate.objects.filter(usuario=request.user).first()
    publicacion.titulo = request.POST['titulo']
    publicacion.descripcion = request.POST['descripcion']
    if request.FILES:
        publicacion.imagen = request.FILES['imagen']

    return publicacion


def mapComentario(request):
    comentario = Cometario()
    comentario.comentario = request.POST['inputComentario']
    comentario.usuario = request.user
    comentario.publicacion = Publicacion.objects.filter(id=request.POST['idPublicacion']).first()
    return comentario

def mapPuntuacion(request):
    puntuacion = Puntuacion()
    puntuacion.puntuacion = request.POST['inputPuntuacion']
    puntuacion.comentario = request.POST['inputComentario']
    puntuacion.usuario = request.user
    escaparate = Escaparate.objects.filter(id=request.POST['idEscaparate']).first()
    escaparate.puntuacionNumerador += Decimal(puntuacion.puntuacion)
    escaparate.puntuacionDenominador += 1
    escaparate.calificacion = escaparate.puntuacionNumerador / escaparate.puntuacionDenominador
    escaparate.save()
    puntuacion.escaparate = escaparate
    return puntuacion

def mapDenuncia(request):
    denuncia = Denuncia()
    denuncia.motivo = request.POST['inputMotivo']
    denuncia.escaparate = Escaparate.objects.filter(id=request.POST['idEscaparate']).first()
    denuncia.usuario = request.user
    return denuncia