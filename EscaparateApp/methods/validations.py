# -*- coding: utf-8 -*-
from EscaparateApp.models import Escaparate, Ciudad, Categoria, Publicacion, Cometario

def validarEscaparate(escaparate):
    if escaparate.nombre == '':
        return 'Nombre invalido'
    if escaparate.descripcion == '':
        return 'Falta descipcion'
    if escaparate.direccion == '':
        return 'Falta direccion'
    return True


