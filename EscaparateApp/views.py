# -*- coding: utf-8 -*-
#facebook sdk pip install -e git+https://github.com/pythonforfacebook/facebook-sdk.git#egg=facebook-sdk
from django.shortcuts import render
import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from EscaparateApp.forms import UserCreationForm, EscaparateForm, PublicacionForm
from EscaparateApp.models import Categoria, Escaparate, Ciudad, Publicacion, Cometario, Puntuacion, ValidateUser
from EscaparateApp.methods.mappers import mapEscaparate, mapPublicacion, mapComentario, mapPuntuacion, mapDenuncia
from django.shortcuts import redirect
from EscaparateApp.methods.utils import sendMail
import facebook
import twitter
#from TwitterAPI import TwitterAPI

def login(request):
    return render(request, 'login.html', {})


def signup(request):
    if request.method == 'GET':
        return render(request, 'accounts/signup.html', {})
    else:
        #import pdb; pdb.set_trace()
        is_anon = True
        if request.user.username != '':
            is_anon = False
            form = UserCreationForm(request.POST, instance = request.user)
        else:
            form = UserCreationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password1']
            email = request.POST['email']
            if is_anon == False:
                user = request.user
            else:
                user = User.objects.create_user(username, email, password)
            user.first_name = request.POST['first_name']
            user.last_name = request.POST['last_name']
            user.set_password(password)
            if is_anon:
                user.is_active = False
            user.save()
            if is_anon:
                sendMail(request, user)
                return render(request, 'home.html', {})
            else:
                return render(request, 'accounts/login.html', {})
        else:
            return render(request, 'accounts/signup.html', {'form': form})


def validarUsuario(request):
    token = request.GET.get('token')
    validateUser = ValidateUser.objects.filter(token=token).first()
    if validateUser == None:
        return render(request, 'home.html')
    user = User.objects.filter(id=validateUser.usuario.id).first()
    user.is_active = True
    user.save()
    validateUser.delete()
    return render(request, 'accounts/login.html', {'valid': True})


@login_required
def modificarperfil(request):
    if request.method == 'GET':
        usuario = UserCreationForm(instance = request.user)
        return render(request, 'accounts/signup.html', {'form': usuario, 'modif': True})
    return render(request, 'home.html')


def home(request):
    categoria = request.GET.get('categoria_id')
    if categoria != None:
        categoria = Categoria.objects.filter(id=categoria).first()
        escaparate = Escaparate.objects.filter(categoria=categoria)
        publicaciones = Publicacion.objects.filter(escaparate=escaparate, fecha_baja = None).order_by('-id')[:10]
        return render(request, 'home.html', {'publicaciones': publicaciones, 'categoria': categoria})
    publicaciones = Publicacion.objects.filter(fecha_baja = None).order_by('-id')[:20]
    return render(request, 'home.html', {'publicaciones': publicaciones})


def escaparate(request):
    miescaparate = False
    bescaparate = request.GET.get('escaparate_id')
    bpublicacion = request.GET.get('publicacion_id')
    publicaciones = None

    if bescaparate != None:
        escaparate = Escaparate.objects.filter(id=bescaparate)
    else:
        if bpublicacion != None:
            publicaciones = Publicacion.objects.filter(id=bpublicacion).first()
            escaparate = Escaparate.objects.filter(id=publicaciones.escaparate.id)
        else:
            escaparate = Escaparate.objects.filter(usuario=request.user)
    if escaparate.count() < 1:
        return redirect('/crearEscaparate/')

    if publicaciones == None:
        publicaciones = Publicacion.objects.filter(escaparate=escaparate, fecha_baja = None).order_by('-id').first()
    comentarios = Cometario.objects.filter(publicacion=publicaciones, fecha_baja = None).order_by('-id')

    if request.user == escaparate.first().usuario:
        miescaparate = True

    return render(request, 'escaparate.html',
    {'escaparate': escaparate.first(),
    'publicaciones': publicaciones,
    'comentarios': comentarios,
    'miescaparate': miescaparate,
    })


@login_required
def crearescaparate(request):
    if request.method == 'GET':
        escaparate = Escaparate.objects.filter(usuario=request.user)
        if escaparate.count() >= 1:
            return render(request, 'crearEscaparate.html', {'escaparate': escaparate.first(), 'ciudades': Ciudad.objects.all(), 'categorias': Categoria.objects.all()})
        return render(request, 'crearEscaparate.html', {'escaparate': escaparate, 'ciudades': Ciudad.objects.all(), 'categorias': Categoria.objects.all()})
    else:
        escaparate = mapEscaparate(request)
        form = EscaparateForm(request.POST)
        if (form.is_valid()):
            escaparate.puntuacionNumerador = 0
            escaparate.puntuacionDenominador = 0
            escaparate.save()
            return redirect('/escaparate/')
        else:
            #no es valido
            return render(request, 'crearEscaparate.html',
            {'escaparate': escaparate,
            'ciudades': Ciudad.objects.all(),
            'categorias': Categoria.objects.all(),
            'form': form,})


@login_required
def crearPublicacion(request):
    escaparate = Escaparate.objects.filter(usuario=request.user).first()
    auth = request.user.social_auth.first()
    if request.method == 'GET':
        form = PublicacionForm()
        bfacebook = False
        if auth != None:
            bfacebook = auth.provider == 'facebook'
        return render(request, 'publicacion.html',
            {'publicacionForm': form,
            'escaparate': escaparate,
            'facebook': bfacebook})
    else:
        form = PublicacionForm(request.POST, request.FILES)
        if form.is_valid():
            publicacion = mapPublicacion(request)
            publicacion.save()
        else:
            return render(request, 'publicacion.html',
            {'publicacionForm': form,
            'escaparate': escaparate})
    if auth != None:
        if auth.provider == 'facebook':
            smessage = publicacion.titulo
            smessage += " - http://escaparate.herokuapp.com/escaparate/?publicacion_id="
            smessage += str(publicacion.id)
            graph = facebook.GraphAPI(auth.extra_data['access_token'])
            graph.put_object("me", "feed", message=smessage)
        #import pdb; pdb.set_trace()
        #api = twitter.api
        #import pdb; pdb.set_trace()
        #api.PostUpdate('I love python-twitter!')
    return redirect('/escaparate/?escaparate_id=' + str(publicacion.escaparate.id))


@login_required
def editarPublicacion(request):
    escaparate = Escaparate.objects.filter(usuario=request.user).first()
    publicacion_id = request.GET.get('publicacion_id')
    publicacion = Publicacion.objects.filter(id = publicacion_id).first()
    form = PublicacionForm(instance = publicacion)
    return render(request, 'publicacion.html',
            {'publicacionForm': form,
            'escaparate': escaparate})

@login_required
def comentarpublicacion(request):
    #import pdb; pdb.set_trace()
    comentario = mapComentario(request)
    comentario.save()
    return redirect('/escaparate/?publicacion_id='+request.POST['idPublicacion'])

@login_required
def puntuarEscaparate(request):
    puntuacion = mapPuntuacion(request)
    puntuacion.save()
    return redirect ('/escaparate/?escaparate_id='+request.POST['idEscaparate'])


@login_required
def denunciarescaparate(request):
    if request.method == 'POST':
        denuncia = mapDenuncia(request)
        denuncia.save()
    escaparateId = request.POST['idEscaparate']
    return redirect('/escaparate/?escaparate_id=' + escaparateId)


@login_required
def eliminarescaparate(request):
    escaparate_id = request.GET.get('escaparate_id')
    escaparate = Escaparate.objects.filter(id=escaparate_id).first()
    escaparate.delete()
    return redirect('/home/')


@login_required
def eliminarcomentario(request):
    comentario_id = request.GET.get('comentario_id')
    publicacion_id = request.GET.get('publicacion_id')
    comentario = Cometario.objects.filter(id=comentario_id).first()
    comentario.fecha_baja = datetime.datetime.now()
    comentario.save()
    return redirect('/escaparate/?publicacion_id=' + publicacion_id)


@login_required
def eliminarpublicacion(request):
    publicacion_id = request.GET.get('publicacion_id')
    publicacion = Publicacion.objects.filter(id=publicacion_id).first()
    idEscaparate = publicacion.escaparate.id
    publicacion.fecha_baja = datetime.datetime.now()
    publicacion.save()
    return redirect('/escaparate/?escaparate_id='+ str(idEscaparate))

def buscar(request):
    from haystack.query import EmptySearchQuerySet, SearchQuerySet
    from haystack.forms import ModelSearchForm
    sqs = SearchQuerySet()
    form = ModelSearchForm(data=request.GET, searchqueryset=sqs, load_all=True)
    if form.is_valid():
        query = form.cleaned_data['q']
    if query:
        resultados = form.search()
    else:
        resultados = EmptySearchQuerySet()
    return render(request, 'resultadoBusqueda.html', { 'resultados': resultados } )

def detalleCalificaciones(request):
    escaparate_id = request.GET.get('escaparate_id')
    escaparate = Escaparate.objects.filter(id=escaparate_id).first()
    puntuaciones =  Puntuacion.objects.filter(escaparate=escaparate).order_by('-id')
    return render(request, 'detalleCalificaciones.html',
    {'escaparate': escaparate,
    'detalle': puntuaciones})