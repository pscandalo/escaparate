# -*- coding: utf-8 -*-

from django import template
from EscaparateApp.models import Categoria, Escaparate, Publicacion, Puntuacion
from EscaparateApp.forms import CustomSearchForm

register = template.Library()


@register.inclusion_tag('menu.html', takes_context=True)
def menu(context):
    request = context.get('request')
    nuevo = False
    if request.user.username != '':
        escaparate = Escaparate.objects.filter(usuario=request.user)
        if escaparate.count() >= 1:
            nuevo = True
    return {'categorias': Categoria.objects.all(),
        'escaparate': nuevo,
        'form': CustomSearchForm}


@register.inclusion_tag('listaPublicaciones.html')
def listaPublicaciones(escaparate):
    return {'publicaciones':
    Publicacion.objects.filter(escaparate=escaparate, fecha_baja = None).order_by('-id')}


@register.inclusion_tag('escaparatesMasPuntuados.html')
def escaparatesMasPuntuados():
    escaparates = Escaparate.objects.order_by('-calificacion')[:10]
    return {'escaparates': escaparates}


@register.inclusion_tag('ultimasCalificaciones.html')
def ultimasCalificaciones():
    calificaciones = Puntuacion.objects.order_by('-id')[:15]
    return {'calificaciones': calificaciones}
