# -*- coding: utf-8 -*-
from haystack import indexes
from EscaparateApp.models import Escaparate, Publicacion


class EscaparateIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    nombre = indexes.EdgeNgramField(model_attr='nombre')
    descripcion = indexes.EdgeNgramField(model_attr='descripcion')

    def get_model(self):
        return Escaparate

    def index_queryset(self, using=None):
        return self.get_model().objects.all()

class PublicacionteIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    titulo = indexes.EdgeNgramField(model_attr='titulo')
    descripcion = indexes.EdgeNgramField(model_attr='descripcion')

    def get_model(self):
        return Publicacion

    def index_queryset(self, using=None):
        return self.get_model().objects.all()