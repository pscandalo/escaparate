# -*- coding: utf-8 -*-
from django import forms
from haystack.forms import ModelSearchForm
from django.contrib.auth.models import User
from EscaparateApp.models import  Escaparate, Publicacion


class CustomSearchForm(ModelSearchForm):
    q = forms.CharField(required=False, label='Buscar', widget=forms.widgets.TextInput(attrs={"class":"form-control", "placeholder":"Buscar..", }))


class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Contraseña', required=True, widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmación de contraseña', required=True, widget=forms.PasswordInput)
    username = forms.CharField(label='Nombre de usuario', max_length=50, required=True)
    first_name = forms.CharField(label='Nombre', max_length=30, required=False)
    last_name = forms.CharField(label='Apellido', max_length=30, required = False)
    email = forms.EmailField(label='Correo Electrónico', max_length=100, required=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name')

    def clean_email(self):
        email = self.cleaned_data.get('email')
        username = self.cleaned_data.get('username')
        if email and User.objects.filter(email=email).exclude(username=username).count():
            raise forms.ValidationError('El email ya fue registrado.')
        return email


    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Las contraseñas no coinciden")
        return password2


class UserChangeForm(forms.ModelForm):
    id = forms.IntegerField(widget = forms.HiddenInput, required=False)
    class Meta:
        model = User
        fields = ('id','username', 'email', 'first_name', 'last_name')

    def clean_password(self):
        return self.initial["password"]


class EscaparateForm(forms.ModelForm):
    nombre = forms.CharField(label='nombre', required=True, max_length=50)
    descripcion = forms.CharField(label='descripcion', required=True, max_length=200)
    direccion = forms.CharField(label='direccion', required=True, max_length=50)
    telefono = forms.CharField(label='telefono', required=True, max_length=50)
    email = forms.CharField(label='email', required=True, max_length=50)
    paginaweb = forms.CharField(label='paginaweb', required=True, max_length=50)
    ciudad = forms.CharField(label='ciudad', required=True, max_length=50)
    categoria = forms.CharField(label='categoria', required=True, max_length=50)

    class Meta:
        model = Escaparate
        fields = ('nombre', 'descripcion', 'direccion','telefono', 'email', 'paginaweb')


class PublicacionForm(forms.ModelForm):

    titulo = forms.CharField(max_length=100,
        widget=forms.TextInput(
            attrs={'class': 'form-control',
            'placeholder': 'Título'}),
        required=True,)

    descripcion = forms.CharField(max_length=1000,
        widget=forms.Textarea(
            attrs={'rows': '5',
            'class': 'form-control',
            'placeholder': 'Descripción'}))

    imagen = forms.ImageField(label='Agregar una imagen..', required=False,)

    id = forms.IntegerField(widget = forms.HiddenInput, required=False,)

    class Meta:
        model = Publicacion
        fields = ('id','titulo','descripcion','imagen')



