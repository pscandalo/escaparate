# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EscaparateApp', '0006_publicacion_imagen'),
    ]

    operations = [
        migrations.AddField(
            model_name='cometario',
            name='fecha_baja',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='publicacion',
            name='fecha_baja',
            field=models.DateTimeField(null=True),
        ),
    ]
