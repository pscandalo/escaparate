# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EscaparateApp', '0002_auto_20150520_0335'),
    ]

    operations = [
        migrations.AddField(
            model_name='escaparate',
            name='calificacion',
            field=models.DecimalField(default=0, max_digits=9, decimal_places=1),
            preserve_default=False,
        ),
    ]
