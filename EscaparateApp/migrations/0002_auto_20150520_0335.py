# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('EscaparateApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=25)),
            ],
        ),
        migrations.CreateModel(
            name='Ciudad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('ciudad', models.CharField(max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Ciudades',
            },
        ),
        migrations.CreateModel(
            name='Cometario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comentario', models.CharField(max_length=200)),
                ('fechaHora', models.DateTimeField(default=django.utils.timezone.now, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Escaparate',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=20)),
                ('descripcion', models.CharField(max_length=100)),
                ('direccion', models.CharField(max_length=50)),
                ('telefono', models.CharField(max_length=20)),
                ('email', models.EmailField(max_length=20)),
                ('paginaweb', models.CharField(max_length=20)),
                ('puntuacionNumerador', models.DecimalField(max_digits=9, decimal_places=1)),
                ('puntuacionDenominador', models.BigIntegerField()),
                ('categoria', models.ForeignKey(blank=True, to='EscaparateApp.Categoria', null=True)),
                ('ciudad', models.ForeignKey(blank=True, to='EscaparateApp.Ciudad', null=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Provincia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('provincia', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Publicacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=100)),
                ('descripcion', models.CharField(max_length=1000)),
                ('fechaHora', models.DateTimeField(default=django.utils.timezone.now, null=True)),
                ('escaparate', models.ForeignKey(to='EscaparateApp.Escaparate')),
            ],
            options={
                'verbose_name_plural': 'Publicaciones',
            },
        ),
        migrations.CreateModel(
            name='Puntuacion',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('puntuacion', models.DecimalField(max_digits=2, decimal_places=1)),
                ('fechaHora', models.DateTimeField(default=django.utils.timezone.now, null=True)),
                ('comentario', models.CharField(max_length=200, null=True, blank=True)),
                ('escaparate', models.ForeignKey(to='EscaparateApp.Escaparate')),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Puntuaciones',
            },
        ),
        migrations.AddField(
            model_name='cometario',
            name='escaparate',
            field=models.ForeignKey(blank=True, to='EscaparateApp.Escaparate', null=True),
        ),
        migrations.AddField(
            model_name='cometario',
            name='publicacion',
            field=models.ForeignKey(blank=True, to='EscaparateApp.Publicacion', null=True),
        ),
        migrations.AddField(
            model_name='cometario',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='ciudad',
            name='provincia',
            field=models.ForeignKey(to='EscaparateApp.Provincia'),
        ),
    ]
