# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('EscaparateApp', '0003_escaparate_calificacion'),
    ]

    operations = [
        migrations.AlterField(
            model_name='escaparate',
            name='descripcion',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='escaparate',
            name='email',
            field=models.EmailField(max_length=50),
        ),
        migrations.AlterField(
            model_name='escaparate',
            name='nombre',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='escaparate',
            name='paginaweb',
            field=models.CharField(max_length=50),
        ),
        migrations.AlterField(
            model_name='escaparate',
            name='telefono',
            field=models.CharField(max_length=50),
        ),
    ]
