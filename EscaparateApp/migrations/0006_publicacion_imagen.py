# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import EscaparateApp.models


class Migration(migrations.Migration):

    dependencies = [
        ('EscaparateApp', '0005_denuncia'),
    ]

    operations = [
        migrations.AddField(
            model_name='publicacion',
            name='imagen',
            field=models.ImageField(null=True, upload_to=EscaparateApp.models.generate_filename, blank=True),
        ),
    ]
