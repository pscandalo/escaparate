# -*- coding: utf-8 -*-
from django.contrib import admin
from EscaparateApp.models import Categoria, Ciudad, Provincia, Denuncia, Publicacion, Escaparate
from django.contrib.auth.models import Group

admin.site.unregister(Group)

class AdminCategoria(admin.ModelAdmin):
    list_display = ('id', 'nombre')

admin.site.register(Categoria, AdminCategoria)


class AdminDenuncia(admin.ModelAdmin):
    list_display = ('id', 'escaparate')

admin.site.register(Denuncia, AdminDenuncia)


class AdminCiudades(admin.ModelAdmin):
    list_display = ('id','ciudad')

admin.site.register(Ciudad, AdminCiudades)


class AdminProvincias(admin.ModelAdmin):
    list_display = ('id','provincia')

admin.site.register(Provincia, AdminProvincias)


class AdminPublicacion(admin.ModelAdmin):
    list_display = ('id','titulo','descripcion')

admin.site.register(Publicacion, AdminPublicacion)

class AdminEscaparate(admin.ModelAdmin):
    list_display = ('id','nombre','descripcion','usuario')

admin.site.register(Escaparate, AdminEscaparate)
