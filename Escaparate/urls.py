from django.conf.urls import include, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin
from django.contrib.auth import views as auth_views

admin.autodiscover()
admin.site.login = login_required(admin.site.login)

urlpatterns = [
    # Examples:
    # url(r'^$', 'Escaparate.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'EscaparateApp.views.home'),
    url(r'^home/$', 'EscaparateApp.views.home', name='home'),

    url(r'^escaparate/$', 'EscaparateApp.views.escaparate',
        name='escaparate'),

    url(r'^crearpublicacion/$', 'EscaparateApp.views.crearPublicacion',
        name='crearPublicacion'),

    url(r'^validarUsuario/$', 'EscaparateApp.views.validarUsuario',
        name='validarUsuario'),

    url(r'^editarpublicacion/$', 'EscaparateApp.views.editarPublicacion',
        name='editarPublicacion'),

    url(r'^denunciarescaparate/$', 'EscaparateApp.views.denunciarescaparate',
        name='denunciarescaparate'),

    url(r'^comentarpublicacion/$', 'EscaparateApp.views.comentarpublicacion',
        name='comentarpublicacion'),

    url(r'^crearescaparate/$', 'EscaparateApp.views.crearescaparate',
        name='crearescaparate'),

    url(r'^accounts/login/$', auth_views.login,
        {'template_name': 'accounts/login.html'}, name='login'),

    url(r'^accounts/signup/$', 'EscaparateApp.views.signup'),

    url(r'^accounts/logout/$', auth_views.logout,
        {'template_name': 'accounts/login.html'}, name='logout'),

    url(r'^puntuarEscaparate/$', 'EscaparateApp.views.puntuarEscaparate',
        name='puntuarEscaparate'),

    url(r'^eliminarpublicacion/$', 'EscaparateApp.views.eliminarpublicacion',
        name='eliminarpublicacion'),

    url(r'^eliminarescaparate/$', 'EscaparateApp.views.eliminarescaparate',
        name='eliminarescaparate'),

    url(r'^eliminarcomentario/$', 'EscaparateApp.views.eliminarcomentario',
        name='eliminarcomentario'),

    url(r'^buscar/$', 'EscaparateApp.views.buscar', name='buscar'),
    #url(r'^accounts/password_reset/$',
        #'EscaparateApp.views.password_reset', name='password_reset'),

    url(r'^detalleCalificaciones/$', 'EscaparateApp.views.detalleCalificaciones',
        name='detalleCalificaciones'),

    url(r'^modificarperfil/$', 'EscaparateApp.views.modificarperfil',
        name='modificarperfil'),

    url(r'^robots\.txt$', include('robots.urls')),

]

from Escaparate import settings
from django.conf.urls import patterns
if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$',
            'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT, }),
    )